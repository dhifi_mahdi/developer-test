<?php

namespace Dev\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('DevTestBundle:Default:index.html.twig', array('name' => $name));
    }
}
