<?php

namespace Dev\TestBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Dev\TestBundle\Entity\User;
use Dev\TestBundle\Form\UserType;

/**
 * User controller.
 *
 */
class UserController extends Controller
{

    /**
     * Lists all User entities.
     *
     */
 
    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DevTestBundle:User')->findAll();

        return $this->render('DevTestBundle:User:list.html.twig', array(
            'entities' => $entities,
           
        ));
    }
   public function deleteAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('DevTestBundle:User')->find($id);
       
            
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }
        $em->remove($entity);
        $em->flush();
        $entities = $em->getRepository('DevTestBundle:User')->findAll();
       
      return $this->render('DevTestBundle:User:list.html.twig', array('entities' => $entities));
    }

    public function editAction($id) {
     
        $em = $this->getDoctrine()->getManager();
        $news = $em->getRepository('DevTestBundle:User')->find($id);
        if (!$news) {
            throw $this->createNotFoundException(
                    'User does not exist' . $id
            );
        }
        $request = $this->getRequest();
        $form = $this->createForm(new UserType(), $news);
        $form->add('submit', 'submit', array('label' => 'Update'));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->flush();
              $entities = $em->getRepository('DevTestBundle:User')->findAll();
               
               return $this->render('DevTestBundle:User:list.html.twig', array('entities' =>   $entities));
           
        }


           
        return $this->render('DevTestBundle:User:edit.html.twig', array(
                    'entity' => $news,
                   
                    'form' => $form->createView()
        ));
    }

    public function addAction() {
        $em = $this->getDoctrine()->getEntityManager();
        $entity = new User();
        $request = $this->getRequest();

        $form = $this->createForm(new UserType(), $entity);
        $form->add('submit', 'submit', array('label' => 'Create'));
        $form->handleRequest($request);
        if ($form->isValid()) {
           

           
            $em->persist($entity);
            $em->flush();
             $entities = $em->getRepository('DevTestBundle:User')->findAll();
              $categories = $em->getRepository('DevTestBundle:User')->findAll();
               return $this->render('DevTestBundle:User:list.html.twig', array(
                    'entities' =>  $entities,
                   'categories'=>$categories,
                   
           ));
        }
         $categories = $em->getRepository('DevTestBundle:User')->findAll();
        return $this->render('DevTestBundle:User:add.html.twig', array(
                    'entity' => $entity,
                    'categories'=>$categories,
                    'form' => $form->createView()
        ));
    }
    
     public function detailAction($id) {
     
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DevTestBundle:User')->find($id);
     
        return $this->render('DevTestBundle:User:detail.html.twig', array(
                    'entity' => $entity,
          
                   
           ));
    }
}
