<?php

/* DevTestBundle:User:list.html.twig */
class __TwigTemplate_4f80c4cc2c8cc3d73b7cf6439eb7081e2d5987c68c3f6832720e5d4bad81df9c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("DevTestBundle::layout.html.twig");

        $this->blocks = array(
            'table' => array($this, 'block_table'),
            'categories' => array($this, 'block_categories'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "DevTestBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_table($context, array $blocks = array())
    {
        echo "             
    <div id=\"page-wrapper\">

            <div class=\"container-fluid\">

                <!-- Page Heading -->
                <div class=\"row\">
                    <div class=\"col-lg-12\">
                        <h1 class=\"page-header\">
                          Users
                        </h1>
                        <ol class=\"breadcrumb\">
                            <li>
                                <i class=\"fa fa-dashboard\"></i>  <a href=\"#\">Dashboard</a>
                            </li>
                            <li class=\"active\">
                                <i class=\"fa fa-table\"></i> Users
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
<div class=\"row\">
                   
           
                 
                  <h2> All Users</h2>   
           
               
                <a href=\"";
        // line 33
        echo $this->env->getExtension('routing')->getPath("users_new");
        echo "\">
                     
                    <button type=\"button\" class=\"btn btn-lg btn-primary\">Add</button>
                </a><br><br>
                
          
      
                       
                        <div class=\"table-responsive\">
                            <table class=\"table table-bordered table-hover table-striped\">
                                <thead>
                                    <tr>
                 <th>N°</th>
                   <th>Id</th>
                <th>Name</th>
                 <th>Date of birth</th>
                   <th>Address</th>
                <th>Description</th> 
                 <th>Created at</th> 
               <th>Actions</th> 
                   </tr>
                    </thead>
                     <tbody>
                     
                                  ";
        // line 57
        $context["i"] = 0;
        // line 58
        echo "                      ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 59
            echo "                    ";
            $context["i"] = ((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) + 1);
            echo "   
            <tr>
                  <td>";
            // line 61
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")), "html", null, true);
            echo "</td>
                <td>";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
            echo "</td>   
                <td>";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "Name", array()), "html", null, true);
            echo "</td>   
                <td>";
            // line 64
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "dob", array()), "Y-m-d"), "html", null, true);
            echo "</td>   
                <td>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "address", array()), "html", null, true);
            echo "</td>   
                <td>";
            // line 66
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "description", array()), "html", null, true);
            echo "</td>
                 <td>";
            // line 67
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "createdAt", array()), "Y-m-d"), "html", null, true);
            echo "</td>
                
             
                <td>
                   
            <a href=\"";
            // line 72
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("users_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">
                    
                                                <button title=\"Details\" type=\"button\" class=\"btn btn-default btn-circle\"><i class=\"fa fa-list-alt\"></i>
                                                </button>
                                            </a>
                                            <a href=\"";
            // line 77
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("users_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">
                                                <button title=\"Edit\" type=\"button\" class=\"btn btn-primary btn-circle\"><i class=\"fa fa-edit\"></i>
                                                </button>
                                            </a>
                                            <button title=\"Delete\" type=\"button\" class=\"btn btn-danger btn-circle\" data-toggle=\"modal\" data-target=\"";
            // line 81
            echo twig_escape_filter($this->env, ("#myModal" . $this->getAttribute($context["entity"], "id", array())), "html", null, true);
            echo "\"><i class=\"fa fa-times\"></i>
                                            </button>
                                            <!-- Modal -->
                                            <div class=\"modal fade\" id=\"";
            // line 84
            echo twig_escape_filter($this->env, ("myModal" . $this->getAttribute($context["entity"], "id", array())), "html", null, true);
            echo "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                                                <div class=\"modal-dialog\">
                                                    <div class=\"modal-content\">
                                                        <div class=\"modal-header\">
                                                            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                            <h4 class=\"modal-title\" id=\"myModalLabel\">delete User ?</h4>
                                                        </div>
                                                        <div class=\"modal-body\">
                                                            Are you sure you want to delete this user? 
                                                        </div>
                                                        <div class=\"modal-footer\">
                                                            <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                                                            <a href=\"";
            // line 96
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("users_delete", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\" title=\"Delete\"><input type=\"submit\" value=\"Delete\" class=\"btn btn-primary\" onclick=\"this.disabled=true;this.value='Deleting, please wait...';this.form.submit();\" /></a>              
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>

                </td>
            </tr>
           
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 108
        echo "                                    

                                </tbody>
                            </table>
                        </div>
                   
                </div>
               
                <!-- /.row -->


                

            </div>
            <!-- /.container-fluid -->

        </div>
      
      
        ";
    }

    // line 128
    public function block_categories($context, array $blocks = array())
    {
        // line 129
        echo "       ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 130
            echo "                              <li>
                                <a href=\"";
            // line 131
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("articles", array("id" => $this->getAttribute($context["category"], "id", array()))), "html", null, true);
            echo "\" >";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "titre", array()), "html", null, true);
            echo "</a>
                            </li>
       ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 134
        echo "                            ";
    }

    public function getTemplateName()
    {
        return "DevTestBundle:User:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 134,  222 => 131,  219 => 130,  214 => 129,  211 => 128,  188 => 108,  170 => 96,  155 => 84,  149 => 81,  142 => 77,  134 => 72,  126 => 67,  122 => 66,  118 => 65,  114 => 64,  110 => 63,  106 => 62,  102 => 61,  96 => 59,  91 => 58,  89 => 57,  62 => 33,  29 => 4,);
    }
}
