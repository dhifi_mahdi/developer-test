<?php

/* DevTestBundle::layout.html.twig */
class __TwigTemplate_b8470f27a56b6749e0bacd3cf1c79dc23ae219c84542b7bb32f2942868300c0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'script' => array($this, 'block_script'),
            'table' => array($this, 'block_table'),
            'form' => array($this, 'block_form'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>

    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta name=\"description\" content=\"\">
    <meta name=\"author\" content=\"\">

    <title>Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("admin/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Custom CSS -->
    <link href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("admin/css/sb-admin.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">

    <!-- Custom Fonts -->
    <link href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("admin/font-awesome/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
        <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->
 ";
        // line 29
        $this->displayBlock('script', $context, $blocks);
        // line 30
        echo " 
</head>

<body>

    <div id=\"wrapper\">

        <!-- Navigation -->
        <nav class=\"navbar navbar-inverse navbar-fixed-top\" role=\"navigation\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-ex1-collapse\">
                    <span class=\"sr-only\">Toggle navigation</span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                    <span class=\"icon-bar\"></span>
                </button>
                <a class=\"navbar-brand\" href=\"index.html\">Dashboard</a>
            </div>
            <!-- Top Menu Items -->
            <ul class=\"nav navbar-right top-nav\">
               
             
                <li class=\"dropdown\">
                    <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-user\"></i> Admin <b class=\"caret\"></b></a>
                   
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class=\"collapse navbar-collapse navbar-ex1-collapse\">
                <ul class=\"nav navbar-nav side-nav\">
                 
              <li>
                        <a href=\"";
        // line 63
        echo $this->env->getExtension('routing')->getPath("users");
        echo "\"><i class=\"fa fa-fw fa-home\"></i>Users List</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

         ";
        // line 71
        $this->displayBlock('table', $context, $blocks);
        // line 73
        echo "                        
                        
     ";
        // line 75
        $this->displayBlock('form', $context, $blocks);
        // line 77
        echo "
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("admin/js/jquery.js"), "html", null, true);
        echo "\"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("admin/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

</body>

</html>

";
    }

    // line 29
    public function block_script($context, array $blocks = array())
    {
        echo " 
        ";
    }

    // line 71
    public function block_table($context, array $blocks = array())
    {
        // line 72
        echo "                        ";
    }

    // line 75
    public function block_form($context, array $blocks = array())
    {
        // line 76
        echo "    ";
    }

    public function getTemplateName()
    {
        return "DevTestBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 76,  155 => 75,  151 => 72,  148 => 71,  141 => 29,  130 => 85,  124 => 82,  117 => 77,  115 => 75,  111 => 73,  109 => 71,  98 => 63,  63 => 30,  61 => 29,  50 => 21,  44 => 18,  38 => 15,  22 => 1,);
    }
}
